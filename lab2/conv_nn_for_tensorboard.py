from mnist import MNIST

# Import MNIST data (Numpy format)
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

import tensorflow as  tf

# Parameters
learning_rate = 0.001
training_epochs = 3
num_steps = 200  # num_steps = 2000
batch_size = 128
display_step = 5

# Network Parameters
n_input = 784  # MNIST data input (img shape: 28*28)
n_classes = 10  # MNIST total classes (0-9 digits)
dropout = 0.75  # Dropout, probability to keep units

# чтение данных
mndata = MNIST(path='db', gz=True)
images, labels = mndata.load_training()
images_t, labels_t = mndata.load_testing()
# index = random.randrange(0, len(images))  # choose an index ;-)
# print(mndata.display(images[index]))
images_number = len(images)
test_images_number = len(images_t)

# prepare database
labels_train = []
for i in range(images_number):
    number = labels[i]
    sample = []
    for j in range(10):
        if j == number:
            sample.append(1.0)
        else:
            sample.append(0.0)
    labels_train.append(sample)

images_train = []
for i in range(images_number):
    sample = []
    for each in images[i]:
        number = each / 255
        sample.append(number)
    images_train.append(sample)

labels_test = []
for i in range(test_images_number):
    number = labels_t[i]
    sample = []
    for j in range(10):
        if j == number:
            sample.append(1.0)
        else:
            sample.append(0.0)
    labels_test.append(sample)

images_test = []
for i in range(test_images_number):
    sample = []
    for each in images_t[i]:
        number = each / 255
        sample.append(number)
    images_test.append(sample)


# Create a dataset tensor from the images and the labels
dataset = tf.data.Dataset.from_tensor_slices(
    (mnist.train.images, mnist.train.labels))
# Automatically refill the data queue when empty
dataset = dataset.repeat()
# Create batches of data
dataset = dataset.batch(batch_size)
# Prefetch data for faster consumption
dataset = dataset.prefetch(batch_size)



graph_conv = tf.Graph()

with (graph_conv.as_default()):
    with tf.Session() as sess:
        # Create an iterator over the dataset
        iterator = dataset.make_initializable_iterator()
        # Initialize the iterator
        sess.run(iterator.initializer)
        # Neural Net Input (images, labels)
        X, Y = iterator.get_next()


    # model
    def conv_net(x, n_classes, dropout, reuse, is_training):
        # Define a scope for reusing the variables
        with tf.variable_scope('ConvNet', reuse=reuse):
            # MNIST data input is a 1-D vector of 784 features (28*28 pixels)
            # Reshape to match picture format [Height x Width x Channel]
            # Tensor input become 4-D: [Batch Size, Height, Width, Channel]
            # x = tf.reshape(x, shape=[-1, 28, 28, 1])

            # Convolution Layer with 32 filters and a kernel size of 5
            conv1 = tf.layers.conv2d(x, 32, 5, activation=tf.nn.relu)
            # Max Pooling (down-sampling) with strides of 2 and kernel size of 2
            conv1 = tf.layers.max_pooling2d(conv1, 2, 2)

            # Convolution Layer with 32 filters and a kernel size of 5
            conv2 = tf.layers.conv2d(conv1, 64, 3, activation=tf.nn.relu)
            # Max Pooling (down-sampling) with strides of 2 and kernel size of 2
            conv2 = tf.layers.max_pooling2d(conv2, 2, 2)

            # Flatten the data to a 1-D vector for the fully connected layer
            fc1 = tf.contrib.layers.flatten(conv2)

            # Fully connected layer (in contrib folder for now)
            fc1 = tf.layers.dense(fc1, 1024)
            # Apply Dropout (if is_training is False, dropout is not applied)
            fc1 = tf.layers.dropout(fc1, rate=dropout, training=is_training)

            # Output layer, class prediction
            out = tf.layers.dense(fc1, n_classes)
            # Because 'softmax_cross_entropy_with_logits' already apply softmax,
            # we only apply softmax to testing network
            out = tf.nn.softmax(out) if not is_training else out

        return out

    # Create a graph for training
    logits_train = conv_net(labels_train, n_classes, dropout, reuse=False, is_training=True)
    # Create another graph for testing that reuse the same weights, but has
    # different behavior for 'dropout' (not applied).
    logits_test = conv_net(labels_test, n_classes, dropout, reuse=True, is_training=False)

    # Define loss and optimizer (with train logits, for dropout to take effect)
    loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
        logits=logits_train, labels=labels_train))
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    train_op = optimizer.minimize(loss_op)

    # Evaluate model (with test logits, for dropout to be disabled)
    correct_pred = tf.equal(tf.argmax(logits_test, 1), tf.argmax(labels_test, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    # Create a summary to monitor cost tensor
    tf.summary.scalar("loss_op", loss_op)
    # Merge all summaries into a single op
    merged_summary = tf.summary.merge_all()

    # Creates a session with log_device_placement set to True.
    # для использования GPU
    config = tf.ConfigProto(log_device_placement=True)
    with tf.Session(config=config) as sess:

        writer = tf.summary.FileWriter('logs', graph_conv)
        # Initializing the variables
        tf.global_variables_initializer().run()

        count = 0
        # Training cycle
        for step in range(1, num_steps + 1):

            # Run optimization
            _, summary = sess.run([train_op, merged_summary])

            if step % display_step == 0 or step == 1:
                # Calculate batch loss and accuracy
                # (note that this consume a new batch of data)
                loss, acc = sess.run([loss_op, accuracy])
                print("Step " + str(step) + ", Minibatch Loss= " + \
                      "{:.4f}".format(loss) + ", Training Accuracy= " + \
                      "{:.3f}".format(acc))

            writer.add_summary(summary, count)
            count += 1

        print("Optimization Finished!")

        writer.close()
