from mnist import MNIST

import tensorflow as  tf

# чтение данных
mndata = MNIST(path='db', gz=True)
images, labels = mndata.load_training()
images_t, labels_t = mndata.load_testing()
# index = random.randrange(0, len(images))  # choose an index
# print(mndata.display(images[index]))
images_number = len(images)
test_images_number = len(images_t)

# prepare database
labels_train = []
for i in range(images_number):
    number = labels[i]
    sample = []
    for j in range(10):
        if j == number:
            sample.append(1.0)
        else:
            sample.append(0.0)
    labels_train.append(sample)

images_train = []
for i in range(images_number):
    sample = []
    for each in images[i]:
        number = each / 255
        sample.append(number)
    images_train.append(sample)

labels_test = []
for i in range(test_images_number):
    number = labels_t[i]
    sample = []
    for j in range(10):
        if j == number:
            sample.append(1.0)
        else:
            sample.append(0.0)
    labels_test.append(sample)

images_test = []
for i in range(test_images_number):
    sample = []
    for each in images_t[i]:
        number = each / 255
        sample.append(number)
    images_test.append(sample)

# print("Test:", images_test[0])
# print("Test:", labels_test[1])

# Parameters
learning_rate = 0.0015
# learning_rate = 0.001
# training_epochs = 15
training_epochs = 20
batch_size = 100
# batch_size = 100
display_step = 1

# Network Parameters
n_hidden_1 = 256  # 1st layer number of neurons
n_hidden_2 = 256  # 2nd layer number of neurons
n_input = 784  # MNIST data input (img shape: 28*28)
n_classes = 10  # MNIST total classes (0-9 digits)


def variable_summaries(var):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev', stddev)
        tf.summary.scalar('max', tf.reduce_max(var))
        tf.summary.scalar('min', tf.reduce_min(var))
        tf.summary.histogram('histogram', var)

graph = tf.Graph()
with graph.as_default():
    # tf Graph input
    X = tf.placeholder("float", [None, n_input], name="X")
    Y = tf.placeholder("float", [None, n_classes], name="Y")

    # Store layers weight & bias
    with tf.name_scope('weights'):
        weights = {
            'h1': tf.Variable(tf.random_normal([n_input, n_hidden_1]), name='h1'),
            'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2]), name='h2'),
            'out': tf.Variable(tf.random_normal([n_hidden_2, n_classes]), name='out')
        }
    with tf.name_scope('biases'):
        biases = {
            'b1': tf.Variable(tf.random_normal([n_hidden_1]), name='b1'),
            'b2': tf.Variable(tf.random_normal([n_hidden_2]), name='b2'),
            'out': tf.Variable(tf.random_normal([n_classes]), name='out')
        }

    # Create model
    def multilayer_perceptron(x):
        # Hidden fully connected layer with 256 neurons
        #layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'], name='layer_1')
        layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
        layer_1 = tf.nn.leaky_relu(layer_1, name='layer_1')
        # Hidden fully connected layer with 256 neurons
        #layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'], name='layer_2')
        layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
        layer_2 = tf.nn.leaky_relu(layer_2, name='layer_2')
        # Output fully connected layer with a neuron for each class
        out_layer = tf.add(tf.matmul(layer_2, weights['out']), biases['out'], name='out_layer')
        return out_layer


    # Construct model
    logits = multilayer_perceptron(X)

    # Define loss and optimizer
    diff = tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=Y)
    with tf.name_scope('total'):
        loss_op = tf.reduce_mean(diff)
    tf.summary.scalar('loss_op', loss_op)

    with tf.name_scope('train'):
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
    train_op = optimizer.minimize(loss_op)


    with tf.name_scope('accuracy'):
        with tf.name_scope('correct_prediction'):
            correct_prediction = tf.equal(tf.argmax(images_test, 1), tf.argmax(labels_test, 1))
        with tf.name_scope('accuracy'):
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    tf.summary.scalar('accuracy', accuracy)
    # Evaluate model (with test logits, for dropout to be disabled)
    variable_summaries(loss_op)

    # Merge all summaries into a single op
    merged_summary = tf.summary.merge_all()

    # Add ops to save and restore all the variables.
    saver = tf.train.Saver()

    # Add meta data
    run_metadata = tf.RunMetadata()

    # Creates a session with log_device_placement set to True.
    # для использования GPU
    config = tf.ConfigProto(log_device_placement=True)
    # with tf.Session(config=config) as sess:

    with tf.Session(config=config) as sess:
        writer = tf.summary.FileWriter('logs', graph)
        # Initializing the variables
        tf.global_variables_initializer().run()

        # Training cycle
        count = 0
        for epoch in range(training_epochs):
            avg_cost = 0.
            total_batch = int(images_number / batch_size)

            for i in range(total_batch):
                batch_i, batch_l = images_train[(i) * batch_size:(i + 1) * batch_size], labels_train[(i) * batch_size:(i + 1) * batch_size]
                _, _, a, c, summary = sess.run([train_op, correct_prediction, accuracy, loss_op, merged_summary], feed_dict={X: batch_i, Y: batch_l}, run_metadata=run_metadata)

                avg_cost += c / total_batch
                writer.add_summary(summary, count)
                count += 1
            # writer.add_summary(summary, epoch)
            # Display logs per epoch step
            if epoch % display_step == 0:
                print("Epoch:", '%03d' % (epoch + 1), "cost={:.9f}".format(avg_cost))


        writer.add_run_metadata(run_metadata, str("cost={:.9f}".format(avg_cost)))
        writer.add_summary(summary, count)


        print("Optimization Finished!")
        writer.flush()
        writer.close()


        # Test model
        # pred = tf.nn.relu(logits)  # Apply softmax to logits
        pred = tf.nn.softmax(logits)  # Apply softmax to logits
        correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(Y, 1))
        # Calculate accuracy
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
        print("Accuracy:", accuracy.eval({X: images_test, Y: labels_test}))

        # проверка работы
        print("\n", mndata.display(images_t[10]))
        print(sess.run([pred], feed_dict={X: images_test[10:11]}))

        # For save
        save_path = saver.save(sess, "model/model.ckpt")
        print("Model saved in path: %s" % save_path)
