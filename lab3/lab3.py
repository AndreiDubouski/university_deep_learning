import tensorflow as tf
from tensorflow.contrib import rnn

# Import MNIST data
from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

'''
To classify images using a recurrent neural network, we consider every image
row as a sequence of pixels. Because MNIST image shape is 28*28px, we will then
handle 28 sequences of 28 steps for every sample.
'''

log_dir = 'logs'

# Training Parameters
learning_rate = 0.001
training_steps = 10000
# training_steps = 10000
batch_size = 128
display_step = 200

# Network Parameters
num_input = 28  # MNIST data input (img shape: 28*28)
timesteps = 28  # timesteps
num_hidden = 128  # hidden layer num of features
num_classes = 10  # MNIST total classes (0-9 digits)

graph = tf.Graph()
with graph.as_default():
    def variable_summaries(var):
        """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
        with tf.name_scope('summaries'):
            mean = tf.reduce_mean(var)
            tf.summary.scalar('mean', mean)
            with tf.name_scope('stddev'):
                stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
            tf.summary.scalar('stddev', stddev)
            tf.summary.scalar('max', tf.reduce_max(var))
            tf.summary.scalar('min', tf.reduce_min(var))
            tf.summary.histogram('histogram', var)


    # tf Graph input
    X = tf.placeholder("float", [None, timesteps, num_input], name="X")
    Y = tf.placeholder("float", [None, num_classes], name="Y")

    # Define weights for BiRNN
    with tf.name_scope('weights'):
        weights = {
            'out': tf.Variable(tf.random_normal([num_hidden, num_classes]))
        }
    # variable_summaries(weights)
    with tf.name_scope('biases'):
        biases = {
            'out': tf.Variable(tf.random_normal([num_classes]))
        }


    def RNN(x, weights, biases):

        # Prepare data shape to match `rnn` function requirements
        # Current data input shape: (batch_size, timesteps, n_input)
        # Required shape: 'timesteps' tensors list of shape (batch_size, n_input)

        # Unstack to get a list of 'timesteps' tensors of shape (batch_size, n_input)
        x = tf.unstack(x, timesteps, 1)

        # Define a lstm cell with tensorflow
        lstm_cell = rnn.BasicLSTMCell(num_hidden, forget_bias=1.0)

        # Get lstm cell output
        outputs, states = rnn.static_rnn(lstm_cell, x, dtype=tf.float32)

        # Linear activation, using rnn inner loop last output
        with tf.name_scope('Wx_plus_b'):
            preactivate = tf.matmul(outputs[-1], weights['out']) + biases['out']
            tf.summary.histogram('pre_activations', preactivate)
        return preactivate


    logits = RNN(X, weights, biases)
    prediction = tf.nn.softmax(logits)

    # Define loss and optimizer
    loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(
        logits=logits, labels=Y))
    with tf.name_scope('train'):
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)

    train_op = optimizer.minimize(loss_op)

    # Evaluate model
    with tf.name_scope('accuracy'):
        with tf.name_scope('correct_prediction'):
            correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
        with tf.name_scope('accuracy'):
            accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
    tf.summary.scalar('accuracy', accuracy)

    # Initialize the variables (i.e. assign their default value)
    init = tf.global_variables_initializer()

    # Merge all the summaries and write them out to
    merged = tf.summary.merge_all()

    # Start training
    config = tf.ConfigProto(log_device_placement=True)
    with tf.Session(config=config) as sess:
        writer = tf.summary.FileWriter(log_dir, graph)
        # Run the initializer
        sess.run(init)

        for step in range(1, training_steps + 1):
            batch_x, batch_y = mnist.train.next_batch(batch_size)
            # Reshape data to get 28 seq of 28 elements
            batch_x = batch_x.reshape((batch_size, timesteps, num_input))
            # Run optimization op (backprop)
            sess.run(train_op, feed_dict={X: batch_x, Y: batch_y})
            if step % display_step == 0 or step == 1:
                run_metadata = tf.RunMetadata()
                # Calculate batch loss and accuracy
                summary, loss, acc = sess.run([merged, loss_op, accuracy], feed_dict={X: batch_x, Y: batch_y},
                                              run_metadata=run_metadata)
                print("Step " + str(step) + ", Minibatch Loss= " + "{:.4f}".format(loss) + ", Training Accuracy= " + \
                      "{:.3f}".format(acc))
                writer.add_run_metadata(run_metadata, 'step%03d' % step)
                writer.add_summary(summary, step)

                if (acc > 0.95):
                    break
        writer.close()

        print("Optimization Finished!")

        # Calculate accuracy for 128 mnist test images
        test_len = 128
        test_data = mnist.test.images[:test_len].reshape((-1, timesteps, num_input))
        test_label = mnist.test.labels[:test_len]
        print("Testing Accuracy:", \
              sess.run(accuracy, feed_dict={X: test_data, Y: test_label}))
