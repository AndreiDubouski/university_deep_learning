import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

"""
Логистическая регрессия
"""


g1 = tf.Graph()
with g1.as_default():
    samples = 160  # количество точек

    # данные для логистической регрессии
    def logistical_f(x, k, b): return 1 / (1 + np.exp(-(x * k + b)))

    # генерируем данные для лошистической регрессии
    #np.random.seed(0)  # делаем случайность предсказуемой
    X = np.linspace(-4, 4, samples)
    #X = np.random.normal(size=samples)
    np.random.shuffle(X)  # перемешать, но не взбалтывать
    Y = (X > 0).astype(np.float)
    X[X > 0] *= 4
    X += .3 * np.random.normal(size=samples)
    X = X[:, np.newaxis]
    X_ = X[:,0]


    def log_regress(data_x, data_y):
        tf_data_x = tf.placeholder(tf.float32, shape=(samples))  # узел на который будем подавать аргументы функции
        tf_data_y = tf.placeholder(tf.float32, shape=(samples))  # узел на который будем подавать значения функции

        k = tf.Variable(initial_value=0.0, dtype=tf.float32, name="k")
        b = tf.Variable(initial_value=0.0, dtype=tf.float32, name="b")
        # model = tf.nn.softmax(tf.matmul(tf_data_x, W) + B)
        model = tf.div(1.0, tf.add(1.0, tf.exp(tf.add(b, tf.multiply(-1.0, tf.multiply(k, tf_data_x))))))#tf.add(tf.multiply(tf_data_x, k), b)

        loss = tf.reduce_mean(tf.square(model - tf_data_y))  # функция потерь: y - delta_y
        optimizer = tf.train.GradientDescentOptimizer(0.025).minimize(loss)  # метод оптимизаци

        with tf.Session() as session:
            writer = tf.summary.FileWriter('logs', session.graph)
            tf.global_variables_initializer().run()

            num_epochs = 800
            for i in range(num_epochs):
                session.run(optimizer, feed_dict={tf_data_x: data_x, tf_data_y: data_y})
            # print("a = %f, b = %f" % (W.eval(), B.eval(),))

            # plt.plot(data_x, list(map(lambda x: weight.eval() * x + bias.eval(), data_x)), data_x, data_y, 'ro')
            x1, x0 = k.eval(), b.eval()

            writer.close()
        return x1, x0


x1, x0 = log_regress(X_, Y)
print("k = %f, b = %f" % (x1, x0,))

# нагенеренные данные для логистической регресии
plt.axes()
plt.ylabel('Y')
plt.xlabel('X')
plt.scatter(X_, Y)
for i in range (samples):
    plt.plot(X_[i], 1/(1+np.exp(x1 * (-1) * X_[i] + x0)), '-ro')  # solid green
plt.grid(True)
plt.show()
plt.interactive(False)