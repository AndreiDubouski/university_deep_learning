import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

#убрать предупреждения об AVX
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

"""
Логистическая регрессия
"""


g3 = tf.Graph()
with g3.as_default():
    samples = 160  # количество точек

    # данные для логистической регрессии
    def poly_f(x): return x * x * 11 - 3 * x - 5

    x_0 = -2  # начало интервала
    x_l = 2  # конец интервала
    sigma = 0.5  # среднеквадратическое отклонение шума

    np.random.seed(0)  # делаем случайность предсказуемой

    data_x = np.arange(x_0, x_l, (x_l - x_0) / samples)  # массив [-2, -1.92, -1.84, ..., 1.92, 2]
    np.random.shuffle(data_x)  # перемешать, но не взбалтывать
    data_y = list(map(poly_f, data_x)) + np.random.normal(0, sigma, samples)  # массив значений функции с шумом

    def log_regress(data_x, data_y):
        tf_data_x = tf.placeholder(tf.float32, shape=(samples))  # узел на который будем подавать аргументы функции
        tf_data_y = tf.placeholder(tf.float32, shape=(samples))  # узел на который будем подавать значения функции

        a2 = tf.Variable(initial_value=0.0, dtype=tf.float32, name="a2")
        a1 = tf.Variable(initial_value=0.0, dtype=tf.float32, name="a1")
        a0 = tf.Variable(initial_value=0.0, dtype=tf.float32, name="a0")

        with tf.name_scope('Model'):
            # Model
            model = tf.add(tf.add(tf.multiply(tf.multiply(tf_data_x, tf_data_x), a2), tf.multiply(a1,tf_data_x)),a0, name="poly_f")

        with tf.name_scope('Loss'):
            # Minimize error using cross entropy
            loss = tf.reduce_mean(tf.square(model - tf_data_y))  # функция потерь: y - delta_y

        with tf.name_scope('SGD'):
            # Gradient Descent
            optimizer = tf.train.GradientDescentOptimizer(0.025).minimize(loss)  # метод оптимизаци

        #with tf.name_scope('Accuracy'):
            # Accuracy
            #acc = tf.equal(tf.argmax(model, 1), tf.argmax(tf_data_y, 1))
            #acc = tf.reduce_mean(tf.cast(acc, tf.float32))

        # Create a summary to monitor cost tensor
        tf.summary.scalar("a2", a2)
        # Create a summary to monitor cost tensor
        tf.summary.scalar("a1", a1)
        # Create a summary to monitor cost tensor
        tf.summary.scalar("a0", a0)

        # Create a summary to monitor cost tensor
        tf.summary.scalar("loss", loss)
        # Create a summary to monitor accuracy tensor
        #tf.summary.scalar("accuracy", acc)
        # Merge all summaries into a single op
        merged_summary = tf.summary.merge_all()

        #loss_array = []
        with tf.Session() as session:
            writer = tf.summary.FileWriter('logs', g3)
            tf.global_variables_initializer().run()

            num_epochs = 80
            for i in range(num_epochs):
                _, c, summary = session.run([optimizer, loss, merged_summary], feed_dict={tf_data_x: data_x, tf_data_y: data_y})

                # Write logs at every iteration
                #tf.summary.scalar('loss', c)

                #tf.summary.scalar('a2', a2)
                #tf.summary.scalar('a1', a1)
                #tf.summary.scalar('a0', a0)

                writer.add_summary(summary, i)


            # plt.plot(data_x, list(map(lambda x: weight.eval() * x + bias.eval(), data_x)), data_x, data_y, 'ro')
            x2, x1, x0 = a2.eval(), a1.eval(), a0.eval()

            writer.close()
        return x2, x1, x0


    #log_regress(data_x, data_y)
    a_2, a_1, a_0 = log_regress(data_x, data_y)
#print("a2 = %f, a1 = %f, a0 = %f" % (a2, a1, a0))

# нагенеренные данные для логистической регресии
plt.axes()
plt.ylabel('Y')
plt.xlabel('X')
plt.scatter(data_x, data_y)

for i in range (samples):
    plt.plot(data_x[i], data_x[i] * data_x[i] * 11 - 3 * data_x[i] - 5, '-ro')  # solid green

for i in range (samples):
    plt.plot(data_x[i], data_x[i] * data_x[i] * a_2 - a_1 * data_x[i] - a_0, '-go')  # solid green


plt.grid(True)
plt.show()
plt.interactive(False)