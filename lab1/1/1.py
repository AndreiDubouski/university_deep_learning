import tensorflow as tf

g0 = tf.Graph()
with g0.as_default():

    # описание параметров:
    # value (первый аргумент) - значение константы
    # shape - размерность. Например: [] - число, [5] - массив из 5 элементов, [2, 3] - матрица 2x3(2 строки на 3 столбца)
    # dtype - используемый тип данных, список возможных https://www.tensorflow.org/api_docs/python/tf/DType
    # name - имя узла. Позволяет дать узлу имя и в дальнейшем находить узел по нему
    a = tf.constant(2.0, shape=[], dtype=tf.float32, name="a")

    # объявляем переменную x
    # при объявлении переменной можно указать достаточно много аргументов
    # на полный список можно взглянуть в документации, скажу только про основные:
    # initial_value - значение переменной после инициализации
    # dtype - тип, name - имя, как и у констант
    x = tf.Variable(initial_value=3.0, dtype=tf.float32, name="x")

    # и объявляем саму операцию умножения, при желании можно так же указать имя
    f = tf.multiply(a, x)  # можно было написать просто f = a*x

    with tf.Session() as session:
        writer = tf.summary.FileWriter('logs', session.graph)

        # прежде всего нужно инициализировать все глобальные переменные
        # в нашем случае это только x
        tf.global_variables_initializer().run()
        # просим вычислить значение узла f внутри сессии
        # в параметре feed_dict передаём значения всех placeholder'ов
        # функция вернёт список значений всех узлов, переданных на выполнение
        result_f, result_a, result_x = session.run([f, a, x])
        print("f = %.1f * %.1f = %.1f" % (result_a, result_x, result_f))
        # print("a = %.1f" % a.eval())  # пока сессия открыта, можно вычислять узлы
        # метод eval похож на метод run у сессии, но не позволяет передать входные данные (параметр feed_dict)

        # print("x = %.1f" % x.eval())
        writer.close()
