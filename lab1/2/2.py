import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

"""
Линейная регрессия
"""


g1 = tf.Graph()
with g1.as_default():
    def f(x): return 2 * x - 3  # искомая функция

    samples = 80  # количество точек

    x_0 = -2  # начало интервала
    x_l = 2  # конец интервала
    sigma = 0.5  # среднеквадратическое отклонение шума

    np.random.seed(0)  # делаем случайность предсказуемой

    data_x = np.arange(x_0, x_l, (x_l - x_0) / samples)  # массив [-2, -1.92, -1.84, ..., 1.92, 2]
    np.random.shuffle(data_x)  # перемешать, но не взбалтывать
    data_y = list(map(f, data_x)) + np.random.normal(0, sigma, samples)  # массив значений функции с шумом


    # """линейная"""
    def tf_lin_regress(data_x, data_y):
        tf_data_x = tf.placeholder(tf.float32, shape=(samples))  # узел на который будем подавать аргументы функции
        tf_data_y = tf.placeholder(tf.float32, shape=(samples))  # узел на который будем подавать значения функции

        k = tf.Variable(initial_value=0.0, dtype=tf.float32, name="k")
        b = tf.Variable(initial_value=0.0, dtype=tf.float32, name="b")
        model = tf.add(tf.multiply(tf_data_x, k), b)

        loss = tf.reduce_mean(tf.square(model - tf_data_y))  # функция потерь: y - delta_y
        optimizer = tf.train.GradientDescentOptimizer(0.025).minimize(loss)  # метод оптимизаци

        with tf.Session() as session:
            writer = tf.summary.FileWriter('logs', session.graph)
            tf.global_variables_initializer().run()

            feed_dict = {tf_data_x: data_x, tf_data_y: data_y}
            session.run([optimizer, loss], feed_dict=feed_dict)  # запускаем оптимизатор и вычисляем "потери"
            print("k = %f, b = %f" % (k.eval(), b.eval(),))
            # plt.plot(data_x, list(map(lambda x: k.eval() * x + b.eval(), data_x)), data_x, data_y, 'ro')
            x1, x0 = k.eval(), b.eval()
            writer.close()
        return x1, x0


    x1, x0 = tf_lin_regress(data_x, data_y)

    plt.axes()
    plt.ylabel('Y')
    plt.xlabel('X')
    plt.scatter(data_x, data_y)
    plt.plot(data_x, data_x * x1 + x0, '-b')  # solid green
    plt.plot(data_x, f(data_x), '-r')  # solid red
    plt.grid(True)
    plt.show()
    plt.interactive(False)
