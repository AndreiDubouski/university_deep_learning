import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import datetime
from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets("MNIST_data", one_hot=True)


learning_rate = 0.0015
training_steps = 2500
training_epochs = 10
batch_size = 128
display_step = 10000
examples_to_show = 10
noise_level = 0.6

# Network Parameters
n_hidden_1 = 196  # 1st layer number of neurons
n_hidden_2 = 64  # 2nd layer number of neurons
n_input = 784  # MNIST data input (img shape: 28*28)
n_classes = 10  # MNIST total classes (0-9 digits)

start_time = datetime.datetime.now()


def variable_summaries(var):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev', stddev)
        tf.summary.scalar('max', tf.reduce_max(var))
        tf.summary.scalar('min', tf.reduce_min(var))
        tf.summary.histogram('histogram', var)


def plot_numpy_array(x):
    """
    Plots the images that are maximally activating the hidden units
    :param x: numpy array of size [input_dim, num_hidden_units]
    """
    fig, axes = plt.subplots(nrows=10, ncols=10, figsize=(17, 17))
    fig.subplots_adjust(hspace=.1, wspace=0)
    img_h = img_w = x.shape[0]
    # img_h = img_w = np.sqrt(x.shape[0]).astype(int)
    for i, ax in enumerate(axes.flat):
        # Plot image.
        ax.imshow(x[:, i].reshape((img_h, img_w)), cmap='gray')
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_yticklabels([])
        ax.set_xticklabels([])
    plt.show()


def plot_images(original_images, noisy_images, reconstructed_images):
    """
    Create figure of original and reconstructed image.
    """
    num_images = original_images.shape[0]
    fig, axes = plt.subplots(num_images, 3, figsize=(9, 9))
    fig.subplots_adjust(hspace=.1, wspace=0)

    img_h = img_w = np.sqrt(original_images.shape[-1]).astype(int)
    for i, ax in enumerate(axes):
        # Plot image.
        ax[0].imshow(original_images[i].reshape((img_h, img_w)), cmap='gray')
        ax[1].imshow(noisy_images[i].reshape((img_h, img_w)), cmap='gray')
        ax[2].imshow(reconstructed_images[i].reshape((img_h, img_w)), cmap='gray')

        # Remove ticks from the plot.
        for sub_ax in ax:
            sub_ax.set_xticks([])
            sub_ax.set_yticks([])

    for ax, col in zip(axes[0], ["Original Image", "Noisy Image", "Reconstructed Image"]):
        ax.set_title(col)

    fig.tight_layout()
    plt.show()


graph = tf.Graph()
with graph.as_default():
    X_noisy = tf.placeholder("float", shape=[None, n_input], name='X_noisy')
    X = tf.placeholder("float", shape=[None, n_input], name="X")
    Y = tf.placeholder("float", shape=[None, 10], name="Y")

    # Store layers weight & bias
    with tf.name_scope('weights'):
        weights = {
            'enc_h1': tf.Variable(tf.random_normal([n_input, n_hidden_1]), name='enc_h1'),
            'enc_h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2]), name='enc_h2'),

            'dec_h1': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_1]), name='dec_h1'),
            'dec_h2': tf.Variable(tf.random_normal([n_hidden_1, n_input]), name='dec_h2'),
        }
    with tf.name_scope('biases'):
        biases = {
            'enc_b1': tf.Variable(tf.random_normal([n_hidden_1]), name='enc_b1'),
            'enc_b2': tf.Variable(tf.random_normal([n_hidden_2]), name='enc_b2'),

            'dec_b1': tf.Variable(tf.random_normal([n_hidden_1]), name='dec_b1'),
            'dec_b2': tf.Variable(tf.random_normal([n_input]), name='dec_b2'),
        }


    # Building the encoder
    def encoder(x):
        # Encoder Hidden layer with sigmoid activation #1
        layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(x, weights['enc_h1']), biases['enc_b1']))
        # Encoder Hidden layer with sigmoid activation #2
        layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, weights['enc_h2']), biases['enc_b2']))
        return layer_2


    # Building the decoder
    def decoder(x):
        # Decoder Hidden layer with sigmoid activation #1
        layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(x, weights['dec_h1']), biases['dec_b1']))
        # Decoder Hidden layer with sigmoid activation #2
        layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, weights['dec_h2']), biases['dec_b2']))
        return layer_2


    # Construct model
    encoder_op = encoder(X_noisy)
    decoder_op = decoder(encoder_op)


    # For perceptron
    def multilayer_perceptron(x):
        # Hidden fully connected layer with 256 neurons
        layer_1 = tf.nn.leaky_relu(tf.add(tf.matmul(x, weights['enc_h1']), biases['enc_b1']))
        layer_2 = tf.nn.leaky_relu(tf.add(tf.matmul(layer_1, weights['enc_h2']), biases['enc_b2']))
        return layer_2


    perceptron = multilayer_perceptron(X)

    # calculate the activation
    # для отображения слоя (значение делим на среднее)
    # axis: The dimensions to reduce.
    enc_w1 = weights['enc_h1']

    # Targets (Labels) are the input data.
    y_true = X

    # Define loss and optimizer
    with tf.variable_scope('Train'):
        with tf.variable_scope('Loss'):
            loss = tf.reduce_mean(tf.losses.mean_squared_error(X, decoder_op), name='loss')
        with tf.variable_scope('Optimizer'):
            optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)

    # Add 5 images from original, noisy and reconstructed samples to summaries
    tf.summary.image('original', tf.reshape(X, (-1, 28, 28, 1)), max_outputs=5)
    tf.summary.image('reconstructed', tf.reshape(decoder_op, (-1, 28, 28, 1)), max_outputs=5)

    # Merge all summaries into a single op
    merged_summary = tf.summary.merge_all()
    with tf.Session() as sess:

        with tf.summary.FileWriter('logs', graph) as writer:

            tf.global_variables_initializer().run()
            # Training cycle
            step = 0
            for epoch in range(training_epochs):

                for i in range(1, training_steps + 1):
                    batch_x, _ = mnist.train.next_batch(batch_size)
                    batch_x_noisy = batch_x + noise_level * np.random.normal(loc=0.0, scale=1.0, size=batch_x.shape)

                    # Run optimization op (backprop) and cost op (to get loss value)
                    _, l, s = sess.run([optimizer, loss, merged_summary],
                                       feed_dict={X_noisy: batch_x_noisy, X: batch_x})
                    step += 1
                    writer.add_summary(s, step)
                    if i % display_step == 0 or i == 1:
                        print(f'Epoch:{epoch}\tStep:{i}\tBatch loss:{l}')

            print("Optimization Finished!")
            writer.flush()
            writer.close()



        noise_level = 0.5
        # Validation
        x_valid = mnist.validation.images
        x_valid_noisy = x_valid + noise_level * np.random.normal(loc=0.0, scale=1.0, size=x_valid.shape)

        feed_dict_valid = {X_noisy: x_valid_noisy, X: x_valid}
        loss_valid, enc_w1 = sess.run([loss, enc_w1], feed_dict=feed_dict_valid)
        print('---------------------------------------------------------')
        print(f"Validation loss: {loss_valid}")


        end_time = datetime.datetime.now()
        print(
            f"h: {(end_time - start_time).seconds % (3600) // (60 * 60)} m:{((end_time - start_time).seconds % 3600) // 60} s:{((end_time - start_time).seconds % 3600 % 60)}")


        # Test the network after training
        # Make a noisy image
        test_samples = 5
        x_test = mnist.validation.images[10:test_samples+10]
        x_test_noisy = x_test + noise_level * np.random.normal(loc=0.0, scale=1.0, size=x_test.shape)
        # Reconstruct a clean image from noisy image
        x_reconstruct = sess.run(decoder_op, feed_dict={X_noisy: x_test_noisy})
        plot_images(x_test, x_test_noisy, x_reconstruct)  # plot images

        x_valid = mnist.validation.images
        y_valid = mnist.validation.labels
        correct_prediction = tf.equal(tf.argmax(perceptron, 1), tf.argmax(Y, 1))  # Testing for Perceptron
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))  # Calculate accuracy
        print("Accuracy:", accuracy.eval({X: x_valid, Y: y_valid}))

        x_valid = mnist.validation.images[119:121]
        y_valid = mnist.validation.labels[119:121]
        print(y_valid)
        print(sess.run([perceptron], feed_dict={X: x_valid}))

        sess.close()
